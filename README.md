# gitlab-runner

Slim Gitlab-Runner Docker Images built with best practices in mind and used in JUIT projects.

The images are based on Debian slim.

Based on the works of https://github.com/FlakM/gitlab-runner-auto-register
