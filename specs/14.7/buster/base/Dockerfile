#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#

FROM debian:buster-slim

ENV RUNNER_VERSION 14.7.1
ENV CLEANER_VERSION 0.0.2

RUN apt-get update -y && \
	apt-get upgrade -y && \
	apt-get install -y \
		jq \
		ca-certificates \
		curl \
		dumb-init \
	&& \
	curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash && \
	apt-get install -y gitlab-runner=${RUNNER_VERSION} && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* && \
	curl -LO https://github.com/FlakM/gitlab-runner-cleaner/releases/download/${CLEANER_VERSION}/gitlab-runner-cleaner.sh && \
	chmod +x gitlab-runner-cleaner.sh

RUN curl -O "https://gitlab-docker-machine-downloads.s3.amazonaws.com/v0.16.2-gitlab.15/docker-machine-Linux-x86_64" && \
     mv docker-machine-Linux-x86_64 /usr/local/bin/docker-machine && \
     chmod +x /usr/local/bin/docker-machine

RUN curl -sLo hetzner.tar.gz $(curl --silent https://api.github.com/repos/JonasProgrammer/docker-machine-driver-hetzner/releases | jq -r '. | first | .assets[] | select(.name|contains("linux_amd64")).browser_download_url') && \
	tar xzf hetzner.tar.gz && \
	chmod +x docker-machine-driver-hetzner && \
	mv docker-machine-driver-hetzner /usr/sbin && \
	rm hetzner.tar.gz

ADD entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint.sh"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]
