#!/usr/bin/env bash
set -Eeuo pipefail

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")/specs"

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
    versions=( */ )
fi
versions=( "${versions[@]%/}" )

suites=( "buster" "bullseye" )

generated_warning() {
	cat <<-EOH
		#
		# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
		#
		# PLEASE DO NOT EDIT IT DIRECTLY.
		#

	EOH
}

gitlabSteps=
for version in "${versions[@]}"; do
    rcVersion="${version%-rc}"

    majorVersion="${rcVersion%%.*}"
    minorVersion="${rcVersion#$majorVersion.}"
    minorVersion="${minorVersion%%.*}"

    apiUrl="https://gitlab.com/gitlab-org/gitlab-runner/-/tags?sort=updated_desc&feed_token=1tyxGo7cb9xNeaJ8WxUs&format=atom"
    apiXqExpr='
        .feed
        | .entry[]
        | select(.title | startswith("v'"$rcVersion"'.") and (contains("'"-rc"'") | not) )
        | [
            .title | ltrimstr("v")
        ]
    '

    if [ "$rcVersion" != "$version" ]; then
        apiXqExpr='
            .feed
            | .entry[]
            | select(.title | startswith("v'"$rcVersion"'.") and contains("'"-rc"'") )
            | [
                .title | ltrimstr("v")
            ]
        '
    fi
    IFS=$'\n'
    possibles=( $(
        curl -fsSL "$apiUrl" \
            | xq --raw-output "$apiXqExpr | @sh" \
            | sort -rV
    ) )
    unset IFS

    if [ "${#possibles[@]}" -eq 0 ]; then
        echo >&2
        echo >&2 "error: unable to determine available releases of $version"
        echo >&2
        continue
    fi

    # format of "possibles" array entries is "VERSION"
    eval "possi=( ${possibles[0]} )"
    fullVersion="${possi[0]}"

    dockerfiles=()

    for suite in "${suites[@]}"; do
        [ -d "$version/$suite" ] || continue
		alpineVer="${suite#alpine}"

		baseDockerfile=Dockerfile-debian.template
		if [ "${suite#alpine}" != "$suite" ]; then
		    baseDockerfile=Dockerfile-alpine.template
        fi

        for variant in base; do
            [ -d "$version/$suite/$variant" ] || continue
            { generated_warning; cat "../templates/$baseDockerfile"; } > "$version/$suite/$variant/Dockerfile"

            echo -n "$fullVersion" > "$version/$suite/$variant/VERSION"
            cp -a \
                ../templates/entrypoint.sh \
                "$version/$suite/$variant/"

            # remove any _extra_ blank lines created by the deletions above
			gawk '
				{
					if (NF == 0 || (NF == 1 && $1 == "\\")) {
						blank++
					}
					else {
						blank = 0
					}

					if (blank < 2) {
						print
					}
				}
			' "$version/$suite/$variant/Dockerfile" > "$version/$suite/$variant/Dockerfile.new"
			mv "$version/$suite/$variant/Dockerfile.new" "$version/$suite/$variant/Dockerfile"

			sed -ri \
				-e 's!%%DEBIAN_TAG%%!'"$suite-slim"'!' \
				-e 's!%%DEBIAN_SUITE%%!'"$suite"'!' \
				-e 's!%%ALPINE_VERSION%%!'"$alpineVer"'!' \
				"$version/$suite/$variant/Dockerfile"
			dockerfiles+=( "$version/$suite/$variant/Dockerfile" )
        done
    done

    (
		set -x
		sed -ri \
			-e 's!%%RUNNER_VERSION%%!'"$fullVersion"'!' \
			"${dockerfiles[@]}"
	)

    newGitlabStep=
    for dockerfile in "${dockerfiles[@]}"; do
		dir="${dockerfile%Dockerfile}"
		dir="${dir%/}"
		variant="${dir#$version}"
		variant="${variant#/}"
		newGitlabStep="\nbuild $version $variant:\n    extends:\n        - .docker-image-build\n    variables:\n        VERSION: '$version'\n        VARIANT: '$variant'"
	done
	gitlabSteps="$gitlabSteps$newGitlabStep"
done

gitlabCi=$(echo "$gitlabSteps" | awk '{gsub(/\\n/, "\n")}1')
echo "$gitlabCi" > ../.gitlab/docker-images.yml
